import numpy as np
from random import randint
import time
import sys
global a
global win_p
global win_o
global flag
global pos
global datac
global name
datac = []
pos = -1
flag = False
win_p = 0
win_o = 1
a = [[-1,-1,-1],[-1,-1,-1],[-1,-1,-1]]
global play
play = -1 #1 - user's turn 2 - Obi's turn
def reset_board():
	global win_p
	global win_o
	global play
	global a
	a = [[-1,-1,-1],[-1,-1,-1],[-1,-1,-1]]
	disp_board()

def disp_board():
	global win_p
	global win_o
	global play
	global a
	print("\n\n\n\t********Board******\n")
	for i in range(len(a)):
		print("\t  ",end = "")
		for j in range(len(a)):
			if a[i][j] == -1:
				print("-",end = "\t")
			elif a[i][j] == 2:
				print("O",end = "\t")
			elif a[i][j] == 1:
				print("X",end = "\t")
		print("\n")
	print("\n\t********Board******\n\n\n")

def start_game():	
	global win_p
	global win_o
	global play
	global a	#toss to choose who starts the game
	print("Toss..")
	print("Choose 1.Head 2.Tails")
	#user's choice
	ch = ''
	while(ch != '1' and ch!= '2'):
		ch = input("->")
	ch = int(ch)
	#print(ch)
	#print(type(ch))
	if (ch !=1) and (ch!=2): #for invalid choice
		print("Invalid Input")
		print("Choose again")
		start_game()		#recursive function
	else: 
		comp = randint(1,2)		
		#print(comp)		#random number generated
		if ch == comp:
			print("You have won the toss! \nWhat do you choose \n1. Play first\n2.Let Obi play First")
			chpl = ''
			while chpl!= '1' and chpl!='2':
				chpl = input("->")
			chpl = int(chpl)

			if (chpl!=1) and (chpl!=2):
				print("Invalid Input.\nWe need to restart the toss.")
				print("Choose again")
				start_game()
			else:
				if chpl == 1:
					play = 1
				else:
					play = 2
		else:
			print("Obi has won the toss\nObi is deciding his choice")
			chpl = randint(1,2)
			if chpl == 2:
				print("Obi has chosen to play first")
				play = 2
			else:
				print("Obi has decided to let you play first")
				play = 1
		move(play)
		disp_board()
		#print(play)

def move(play):

	global win_p
	global win_o
	global a
	if play == 1:
		user_move()
	if play ==2 :
		obi_move()
	game_over()

def user_move():
	global win_p
	global win_o
	global play
	global a
	#user's turn
	print("Your move next. Enter a number between 1 and 9 where you want to move.")
	mov = ''
	while mov != '1' and mov != '2' and mov != '3' and mov != '4' and mov != '5' and  mov != '6' and  mov != '7' and mov != '8' and mov != '9': 
		mov = input("->")
	mov = int(mov)	
	mv = get_move(mov)
	if mv == -1:
		print("Invalid move,\n Please try again")
		user_move()
	m = mv[0]
	n = mv[1]
	if a[m][n] != -1:
		print("You cannot move there!\nMove again!")
		user_move()
	else:
		print("Moved!")
		a[m][n] = 1
		disp_board()
		play = 2
		game_over()
		move(play)
			

def get_move(mov):
	global win_p
	global win_o
	global play
	global a
	mv = (-1,-1)
	if mov <=0 or mov >=10:
		return -1
	if mov == 1:
		mv = (0,0)
	elif mov == 2:
 		mv = (0,1)
	elif mov == 3:
 		mv = (0,2)
	elif mov == 4:
 		mv = (1,0)
	elif mov == 5:
 		mv = (1,1)
	elif mov == 6:
 		mv = (1,2)
	elif mov == 7:
 		mv = (2,0)
	elif mov == 8:
 		mv = (2,1)
	elif mov == 9:
 		mv = (2,2)
	return mv

def obi_move():
	global win_p
	global win_o
	global play
	global a
	play = 1
	
	count_moves = 0
	for i in range(3):
		for j in range(3):
			if a[i][j] == 1:
				count_moves += 1
	if count_moves <=1 :
		mov = randint(1,9)
		mv = get_move(mov)
		m = mv[0]
		n = mv[1]
		if a[m][n] != -1:
			obi_move()
		else:
			a[m][n] = 2			
			t = "Obi is thinking\n"
			for l in t:
				sys.stdout.write(l)
				sys.stdout.flush()
				time.sleep(0.2)
			print("Obi has moved.")
			disp_board()
			play = 1
			game_over()
			move(play)
	else:
		ms = False
		if ms == False:
			count = 0
			for i in range(len(a)):
				if a[i][i] == 2:
					count = count+1
				if count == 2:
					for j in range(3):
						if a[j][j] == -1:
							a[j][j] = 2
							ms = True
							break

		if ms == False:
			count = 0
			for i in range(len(a)):
				if a[2-i][i] == 2:
					count = count+1
				if count == 2:
					for j in range(3):
						if a[2-j][j] == -1:
							a[2-j][j] = 2
							ms = True
							break

		if ms == False:					
			for i in range(len(a)):
				count = 0
				for j in range(len(a)):
					if a[i][j] == 2:
						count = count+1
				if count == 2:
					for r in range(3):
						if a[i][r] == -1:
							a[i][r] = 2
							ms = True
							break
		if ms == False:
			for i in range(len(a)):
				count = 0
				for j in range(len(a)):
					if a[j][i] == 2:
						count = count+1
				if count == 2:
					for r in range(3):
						if a[r][i] == -1:
							a[r][i] = 2
							ms = True
							break

		if ms == False:
			count = 0
			for i in range(len(a)):
				if a[i][i] == 1:
					count = count+1
				if count == 2:
					for j in range(3):
						if a[j][j] == -1:
							a[j][j] = 2
							ms = True
							break

		if ms == False:
			count = 0
			for i in range(len(a)):
				if a[2-i][i] == 1:
					count = count+1
				if count == 2:
					for j in range(3):
						if a[2-j][j] == -1:
							a[2-j][j] = 2
							ms = True
							break

		if ms == False:					
			for i in range(len(a)):
				count = 0
				for j in range(len(a)):
					if a[i][j] == 1:
						count = count+1
				if count == 2:
					for r in range(3):
						if a[i][r] == -1:
							a[i][r] = 2
							ms = True
							break
		if ms == False:					
			for i in range(len(a)):
				count = 0
				for j in range(len(a)):
					if a[j][i] == 1:
						count = count+1
				if count == 2:
					for r in range(3):
						if a[r][i] == -1:
							a[r][i] = 2
							ms = True
							break


		if ms == False:
			mov = randint(1,9)
			mv = get_move(mov)
			m = mv[0]
			n = mv[1]
			if a[m][n] != -1:
				obi_move()
			else:
				a[m][n] = 2
				t = "Obi is thinking\n"
				for l in t:
					sys.stdout.write(l)
					sys.stdout.flush()
					time.sleep(0.2)
				print("Obi has moved.")
				disp_board()
				play = 1
				game_over()
				move(play)
		if ms == True:
			play = 1
			
			t = "Obi is thinking\n"
			for l in t:
				sys.stdout.write(l)
				sys.stdout.flush()
				time.sleep(0.2)
			disp_board()
			game_over()
			move(play)
			print("Obi has moved.")

def game_over():
	global win_p
	global win_o
	global matches
	global play
	global a
	win = False
		
	count = 0
	if win == False:
		for i in range(len(a)):
			if a[i][i] == 1:
				count = count+1
			if count == 3:
				print("Congratulations, you have beaten Obi.")
				win_p += 1
				win = True

	if win == False:
		count = 0
		for i in range(len(a)):
			if a[i][i] == 2:
				count = count+1
			if count == 3:
				print("Obi has won the game\nBetter luck next time.")
				win_o += 1
				win = True

	if win == False:			
		count = 0
		for i in range(len(a)):
			if a[2-i][i] == 2:
				count = count+1
			if count == 3:
				print("Obi has won the game\nBetter luck next time.")
				win_o += 1
				win = True

	if win == False:
		count = 0
		for i in range(len(a)):
			if a[2-i][i] == 1:
				count = count+1
			if count == 3:
				print("Congratulations, you have beaten Obi.")
				win_p += 1
				win = True

	if win == False:
		for i in range(len(a)):
			count = 0
			for j in range(len(a)):
				if a[i][j] == 1:
					count = count+1
			if count == 3:
				print("Congratulations, you have beaten Obi.")
				win_p += 1
				win = True
				break
	if win == False:
		for i in range(len(a)):
			count = 0
			for j in range(len(a)):
				if a[i][j] == 2:
					count = count+1
			if count == 3:
				print("Obi has won the game\nBetter luck next time.")
				win_o += 1
				win = True
				break

	if win == False:
		for i in range(len(a)):
			count = 0
			for j in range(len(a)):
				if a[j][i] == 2:
					count = count+1
			if count == 3:
				print("Obi has won the game\nBetter luck next time.")
				win_o += 1
				win = True
				break

	if win == False:
		for i in range(len(a)):
			count = 0
			for j in range(len(a)):
				if a[j][i] == 1:
					count = count+1
			if count == 3:
				print("Congratulations, you have beaten Obi.")
				win_p += 1
				win = True
				break

	count = 0
	for i in range(3):
		for j in range(3):
			if a[i][j] == -1:
				count = count+1
	if count == 0:
		print("Game draw! Obi is as strong as you!")
		win = True

	if win == True:
		print("Number of games won:\nYou:",win_p)
		print("Obi:",win_o)
		
		matches = matches + 1
		print("Number of games played:",matches)
		print("\n")
		print("Do you want to play again!\n1.Yes!! \n2. Later.")
		che = ''
		while (che !='1') and (che!='2'):
			che = input("->")
		che = int(che)

		if che == 2:
			end()
		else:
			reset_board()
			start_game()


def user(start):
	global win_p
	global win_o
	global flag
	global pos
	global name
	global datac
	global matches
	print("Enter your name:")
	name = input("->")
	
	data = open("save.txt")
	datac = data.readlines()
	for i in range(len(datac)):
		datac[i]  = datac[i].replace("\n","")
	for i in range(len(datac)):
		datac[i] = datac[i].split(",")
		datac[i][2] = int(datac[i][2])
		datac[i][1] = int(datac[i][1])
		datac[i][2] = int(datac[i][3])

	name = name.lower()
	flag = False
	pos = -1
	length = len(datac)
	for i in range(len(datac)):
		if name == datac[i][0]:
			pos = i
			flag = True
	if flag == True:
		print("Continue from previously saved history?\n1.Yes\n2.No")
		ch = ''
		while ch != '1' and ch!='2':
			ch = input("->")

		ch = int(ch)
		if ch == 2:
			datac[i][1] = 0
			datac[i][2] = 0
			datac[i][3] = 0
			win_p = 0
			win_o = 0
			matches = 0
		else :
			win_o = datac[pos][2]
			win_p = datac[pos][1]
			matches = int(datac[pos][3])
	else:
		win_o = 0
		win_p = 0
		matches = 0

	print("Number of games won:\nYou:",win_p)
	print("Obi:",win_o)
	print("Number of matches played:",matches)
	print("\n")
	
	start_game()



		
def end():
	global flag
	global pos
	global name
	global matches
	data = open("save.txt")
	dc = data.readlines()
	if flag == True:
		datac[pos][1] = win_p
		datac[pos][2] = win_o
		datac[pos][3] = matches
	else:
		newentry = []
		newentry.append(name)
		newentry.append(win_p)
		newentry.append(win_o)
		newentry.append(matches)
	data.close()
	data = open("save.txt","w")
	length = len(datac)
	for i in range(length):
		for j in range(4):
			datac[i][j] = str(datac[i][j])
		datac[i] = ",".join(datac[i])
	full_data = "\n".join(datac)
	data.write(full_data)
	data.close()
	print("Thank you for playing with Obi\nObi is happy!")
	print("Good day")
	blah = "Obi is going to sleep now!"
	for l in blah:
		sys.stdout.write(l)
		sys.stdout.flush()
		time.sleep(0.2)
	exit(0)

matches = 0
name = ''
flag = False
pos = -1
start = True
user(start)
